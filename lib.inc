%define space_code 0x20
%define newline_code 0xA
%define tab_code 0x9
%define zero_ascii_code 0x30

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov  rax, 60
    xor  rdi, rdi          
    syscall

; rdi
; rsi
; rdx
; rcx

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; rdi - string address
string_length:
    xor rax, rax
    .count:
      cmp byte [rdi+rax], 0
      je .end
      inc rax
      jmp .count
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi - string address
print_string:
    push rdi
    call string_length
    pop rdi
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov  rsi, rsp
    mov  rdx, 1
    mov  rax, 1
    mov  rdi, 1
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, newline_code
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r8
    mov r8, rsp
    xor rax, rax
    xor rsi, rsi
    
    mov rax, rdi
    dec rsp
    mov byte [rsp], 0
    .loop:
        xor rdx, rdx
        mov rdi, 10
        div rdi
        add dl, zero_ascii_code
        dec rsp
        mov byte [rsp], dl
        test rax, rax
        je .end
        jmp .loop
    .end:
        mov rdi, rsp
        call print_string
        mov rsp, r8
        pop r8
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    test rax, rax
    js .negative
    call print_uint
    jmp .end

    .negative: 
    neg rax
    push rax
    mov rdi, '-'
    call print_char
    pop rdi
    call print_uint

    .end:
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - 1 str ptr
; rsi - 2 str ptr
string_equals:
    xor rax, rax

    call string_length

    mov r9, rdi
    mov rdx, rax
    mov rdi, rsi

    call string_length
    ; rdx && rax contain lengths
    cmp rdx, rax
    jne .b_end

    xor rcx, rcx
    xor rax, rax
    xor rsi, rsi
    
    test rdx, rdx
    je .g_end
    dec rdx
    .loop1:
    ; r9 && rdi contain str pointers
        mov al, byte [rdi + rcx]
        mov sil, byte [r9 + rcx]

        ; if not eq jump
        cmp rax, rsi
        jne .b_end
        
        cmp rdx, rcx
        je .g_end

        inc rcx
        jmp .loop1
    .g_end:
        mov rax, 1
        ret
    .b_end:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

; rdi - buffer start
; rsi - buffer size
read_word:
    mov r8, rdi
    mov r9, rsi
    xor r10, r10

    .skip_tabs:
        call read_char

        cmp rax, space_code
        je .skip_tabs
        cmp rax, tab_code
        je .skip_tabs
        cmp rax, newline_code
        je .skip_tabs
        ; skipped all tabulation
    .loop:
        cmp r10, r9
        je .bad_end
        
        cmp rax, space_code
        je .end
        cmp rax, tab_code
        je .end
        cmp rax, newline_code
        je .end
        test rax, rax
        je .end

        mov byte [r8 + r10], al

        inc r10
        call read_char
        jmp .loop
    .bad_end:
        xor rax, rax
        ret
    .end:
        ; null terminator & size + 1
        mov rax, r8
        mov rdx, r10
        mov byte [r8 + r10], 0
        ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    xor rax, rax
    xor rbx, rbx
    xor rsi, rsi ;rsi - length
    ;rdi - start

    .loop:
        xor rbx, rbx
        mov bl, byte [rdi + rsi]
        sub bl, zero_ascii_code ; transformed to digit
        test bl, bl
        jl .g_end
        cmp bl, 9
        jg .g_end
        
        mov rcx, 10
        mul rcx
        add rax, rbx
        inc rsi
        jmp .loop
    .bad_end:
        xor rdx, rdx
        pop rbx
        ret
    .g_end:
        test rsi, rsi
        je .bad_end
        mov rdx, rsi
        pop rbx
        ret
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte [rdi]
    cmp al, '-'
    je .neg
    call parse_uint
    ret
    .neg:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx
        je .b_end
        mov r8b, byte [rdi + rax]
        mov byte [rsi + rax], r8b
        test r8b, r8b
        je .g_end
        inc rax
        jmp .loop

    .b_end:
        xor rax, rax
        ret
    .g_end:
        ret
